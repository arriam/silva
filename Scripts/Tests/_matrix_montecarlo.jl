# Тестиование квадратной матрицы парных расстояний на соответствие словарю (в порядке словаря)
# Args:
# 1 - matrix name
# 2 - n lines
# 3 - .fasta name

include("./../CommonModules.jl/PDist.jl")
using ProgressMeter

function main(ARGS)
    
    mtr_name = ARGS[1]
    n_max = parse(Int, ARGS[2])
    fasta_name = ARGS[3]
    
    dic = PDist.read_FASTA(fasta_name, n_max)
    line = zeros(Float32, n_max)
    open(mtr_name, "r") do f
        @showprogress for i=1:n_max
            PDist.parceline!(Float32, f, line, ",")
            @inbounds Threads.@threads for j=1:n_max%500 # Каждый 500-й элемент
                r = rand(1:n_max)
                @assert PDist.pdist(Float32, dic, i,r) == line[r] "($i, $r)"
            end
        end
        println("tests passed!")
    end
end


main(ARGS)