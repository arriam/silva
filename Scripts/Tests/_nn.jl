# Тестиование матриц ближайших соседей
# Args:
# 1 - nn matrix name
# 2 - nn lines
# 3 - width
# 4 - nn ids name
# 5 - .fasta name

using ProgressMeter
using CSV
using DataFrames
include("./../../Scripts/CommonModules.jl/PDist.jl")


function main(ARGS)
    dists_name = ARGS[1]
    ids_names = ARGS[4]
    n_max = parse(Int, ARGS[2])
    width = parse(Int, ARGS[3])
    withrandids = false
    randids = Int64[]
    
    println("Enter additional list of ids:")
    ans = readline()
    if !(ans == "")
        open(ans, "r") do f
            randids = PDist.parceline(Int, f, ",")
            withrandids = true
        end
    end
    
    println("Чтение матриц...")
    dists = Matrix(DataFrame(CSV.File(dists_name, types=Float32, header=false)))
    ids = Matrix(DataFrame(CSV.File(ids_names, types=Int64, header=false)))
    
    
    fastaname = ARGS[5]
    dic = PDist.read_FASTA(fastaname, n_max)
    
    @showprogress "Checking for missorting" for i=1:259898
        @assert issorted(dists[i, :]) "Bad sorting at line $i"
    end
    println("All distances are sorted!")
    
    if !withrandids
        onlyids = Set(ids)
        @assert Set(ids) == Set(randids) "ids matrix and randids mismatch"
        println("ids are coherent with randids!")
    end
            
        
    @showprogress "Checking for distance coherence" for stolb=1:width
        Threads.@threads for i=1:n_max%50
            stroka = rand(1:n_max)
            @assert PDist.pdist(Float32,dic,stroka,ids[stroka,stolb])==dists[stroka,stolb] "Wrong distance at $stroka, $stolb"
        end
    end
    println("Seems, everything is OK!")
end


main(ARGS)
