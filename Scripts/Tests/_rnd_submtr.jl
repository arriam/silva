# Проверка малой матрицы случайных последовательностей
# ARGS:
# 1 - matrix name
# 2 - ids name
# 3 - fastaname
# 4 - n_max

include("./../../Scripts/CommonModules.jl/PDist.jl")
using DelimitedFiles
using LinearAlgebra
using ProgressMeter

function main(ARGS)
    println("=====Script for checking random submatrix coherence=====")
    println("Reading files from disc")
    mtr = readdlm(ARGS[1], ',', Float32)
    @assert issymmetric(mtr) "Matrix is not symmetric"
    ids = vec(readdlm(ARGS[2], ',', Int))
    @assert length(ids) == size(mtr)[1] "Matrix size and ids length missmatch"
    dic = PDist.read_FASTA(ARGS[3], parse(Int, ARGS[4]))
    
    @showprogress "Checking values" for j=1:length(ids)
        @inbounds Threads.@threads for i=1:length(ids)
            @assert mtr[i, j] == PDist.pdist(Float32, dic, ids[i], ids[j]) "Incorrect value at [$i, $j]"
        end
    end
    println("Seems everythyng is OK!")
end

main(ARGS)