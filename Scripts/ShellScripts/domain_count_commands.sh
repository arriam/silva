# Команды для подсчёта вхождений:

grep -c '>' filename.fasta                 # Всего сиквенсов

grep -c '[0-9] Bacteria;' filename.fasta   # Сиквенсов бактерий
grep -c '[0-9] Eukaryota;' filename.fasta  # Сиквенсов эукариот
grep -c '[0-9] Archaea;' filename.fasta    # Сиквенсов архей