using Distributed
@everywhere using DelimitedFiles
@everywhere using Random


@everywhere function removeRow(sym_arr::Matrix, n::Int)::Matrix
    s = size(sym_arr)[1]
    v = vec(copy(sym_arr))
    return reshape(
        deleteat!(
            deleteat!(v, n:s:s*s),
        (n-1)*(s-1)+1:n*(s-1)),
    (s-1,s-1))
end


@everywhere function removeRow(sym_arr::AbstractMatrix, n::Int)::Matrix
    s = size(sym_arr)[1]
    v = vec(Matrix(copy(sym_arr)))
    return reshape(
        deleteat!(
            deleteat!(v, n:s:s*s),
        (n-1)*(s-1)+1:n*(s-1)),
    (s-1,s-1))
end


@everywhere function getSums(array::Array{Int8})::Vector{UInt16}
    s = size(array)[1]
    sums = zeros(UInt16, s)
    for (i, col) in enumerate(eachcol(array)) # PARALLEL
        sums[i] = sum(col)
    end
    sums
end


@everywhere function convert201(data::Matrix, lower, upper)::Array{Int8}
    dims = size(data)[1]
    Int8[ lower<=data[i,j]<=upper ? 1 : 0 for i=1:dims, j=1:dims ]
end


@everywhere function findSimplex(matrix::Matrix)::Int64
    mtr = copy(matrix)
    sums = getSums(mtr)
    low = minimum(sums)
    while low+1 < length(sums)
        lows = findall(i->i==low, sums)
        id = rand(lows)
        mtr = removeRow(mtr, id)
        sums = getSums(mtr)
        low = minimum(sums)
    end
    size(mtr)[1]
end


@everywhere function writeLine(name, matrix, n_max)
    line = [findSimplex(matrix) for i=1:n_max]
    writedlm( name,  line', ',')
end


function catDel(n_max,foldername)
    all = Int[]
    for i = 1:n_max
        name = "$foldername/"*lpad(i,2,"0")*".csv"
        v = vec(readdlm(name, ',', Int16))
        append!(all, v)
    end
    all
end


function exeDistr(tempfolder, matrix, n, nprocs)
    jpp = n ÷ nprocs  #jobs per process
    @sync @distributed for i=1:nprocs
        writeLine(tempfolder*"/"*lpad(i,2,"0")*".csv", mtr, jpp)
    end
    
    catDel(nprocs, tempfolder)
end


function main()
    data = readdlm("/home/uzumymw/data/other/5kRND/p_matrix5000RAND.csv",',', Float16);
    @everywhere mtr = convert201($data, .251, .269)

    tte =  3200
    nwrk = nworkers()

    @everywhere tempfolder = "temp_" * randstring('a':'z',8)
    mkdir(tempfolder)
    @time all = exeDistr(tempfolder, mtr, tte, nwrk)

    name_on_disc = string(size(mtr)[1]÷1000) * "k_2_" * string(length(all))*".csv"
    writedlm( name_on_disc,  all', ',')
    rm(tempfolder, recursive=true)
end

main()