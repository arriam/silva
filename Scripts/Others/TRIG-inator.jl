include("../P_dist/Constants.jl")
include(Constants.common_lib)
using ProgressMeter


println("How many lines?")
n = parse(Int, readline()) ÷ 1000

open(Constants.filename_out*"$n"*"K.csv", "r") do filein
    open(Constants.filename_out*"$n"*"K_TOP_triang.csv", "w") do fileout
        @showprogress for i=1:n*1000
            line = split(readline(filein), ',')
            line[1:i] .= "0.0"
            write(fileout, join(line[:], ",")*'\n')
        end
    end
    println("Success!")
end
