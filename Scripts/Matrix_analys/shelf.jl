#Args:
#1 - matrix name
#2 - n lines in matrix
#3 - n bars in histogramm

include("./../CommonModules.jl/PDist.jl")
using ProgressMeter
using DelimitedFiles

function compute_square!(file_handle, n_lines, n_shelfs, stats)
    line = zeros(Float32, n_lines)
    @inbounds @showprogress for i=1:n_lines
            PDist.parceline!(Float32, file_handle, line, ",")
            Threads.@threads for j=i+1:n_lines
                stats[trunc(Int64, fma(line[j], n_shelfs, 1))] += 1
            end
        end
end

function compute_rect!(file_handle, dims, n_shelfs, stats)
    n_lines, width = dims
    line = zeros(Float32, width)
    @inbounds @showprogress for i=1:n_lines
            line .= PDist.parceline(Float32, file_handle, ",")[1:width]
            Threads.@threads for j=1:width
                stats[trunc(Int64, fma(line[j], n_shelfs, 1))] += 1
            end
        end
end
    



function main(ARGS)
    println("=====Script for extracting probability distribution of distances in matrix=====")
    n_shelfs = parse(Int, ARGS[3])
    mtr_name = ARGS[1]
    n_lines = width = parse(Int, ARGS[2])
    stats = zeros(Int64, n_shelfs)
    println("Is your matrix square?\n[yes]|width")
    answer = readline()
    if !(lowercase(answer) in ["yes", "y", "да", "д", ""])
        width = parse(Int, answer)
        open(mtr_name, "r") do f
            println("Start!")
            compute_rect!(f, (n_lines, width), n_shelfs, stats)
        end
    else
        open(mtr_name, "r") do f
            println("Start!")
            compute_square!(f, n_lines, n_shelfs,stats) 
        end
    end
    println("Computations completed!")
    total = sum(stats)
    prob_distr = Float32[stats[i]*n_shelfs/total for i=1:n_shelfs]
    writedlm("pd_$(n_lines)x$(width)_P-density_$(n_shelfs)bars.csv", prob_distr', ',')
end

main(ARGS)