# Почанковый расчёт и запись матрицы парных расстояний (двойная работа)
# ARGS:
# 1 - fastaname
# 2 - n_max
# 3 - chunk size

include("./../CommonModules.jl/PDist.jl")
using ProgressMeter
using DelimitedFiles



function calculate_chunk!(chunk, dictionary, n_line)
    h, w = size(chunk)
    Threads.@threads for j=1:h
        @inbounds @simd for i=1:w
            chunk[j, i] = PDist.pdist(Float32, dictionary, n_line-1+j, i)
        end
    end
end


function main(ARGS)
    fastaname = ARGS[1]
    n_max = parse(Int, ARGS[2])
    chunk_size = parse(Int, ARGS[3])
    @assert endswith(fastaname, r".fasta(.gz)?")
    dic = PDist.read_FASTA(fastaname, n_max)
    
    
    chunk = zeros(Float32, chunk_size, n_max)
    n_total_chunks = n_max ÷ chunk_size
    n_rest = n_max % chunk_size

    open("mtr_$n_max.csv", "w") do f
        @showprogress for i=1:n_total_chunks
            calculate_chunk!(chunk, dic, 1+(i-1)*chunk_size)
            writedlm( f,  chunk, ',')
        end
        
        println("Last bits...")
        chunk = chunk[1:n_rest, :]
        calculate_chunk!(chunk, dic, n_max-n_rest+1)
        writedlm( f,  chunk, ',')

    end
end


@time main(ARGS)
