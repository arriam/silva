# Создание малой матрицы из случайных последовательностей
# ARGS:
# 1 - fastaname
# 2 - n_max
# 3 - n submatrix

include("./../../Scripts/CommonModules.jl/PDist.jl")
using DelimitedFiles
using LinearAlgebra
using Random


function p_smol(dic, rand_ids)
    n = length(rand_ids)
    matrix = zeros(Float32, n,n)
    Threads.@threads for j=1:n
        @inbounds @simd for i=j+1:n
            matrix[i, j] = PDist.pdist(Float32, dic, rand_ids[i], rand_ids[j])
        end
    end
    matrix
end


function main(ARGS)
    println("=====Script for creating small random submatrix=====")
    fastaname = ARGS[1]
    n_max = parse(Int, ARGS[2])
    n = parse(Int, ARGS[3])
    
    dic = PDist.read_FASTA(fastaname, n_max)
    
    println("Shuffling...")
    rand_ids = shuffle([1:n_max;])[1:n]
    
    println("Computing matrix...")
    @timev matrix = Matrix(Symmetric(p_smol(dic, rand_ids), :L))
    
    println("Writing to disc...")
    @time writedlm( "$(n)rnd_of_$(n_max)mtr.csv",  matrix, ',')
    @time writedlm( "$(n)rnd_of_$(n_max)ids.csv",  rand_ids', ',')
    
    println("The end.")
end

main(ARGS)