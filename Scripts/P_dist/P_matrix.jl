# Old version with RAM-heavy distance() function

using FastaIO
using LinearAlgebra
using DelimitedFiles
using CSV
using Distributed
using ProgressMeter


# Loading sequences to RAM as Vectors of Int8.

function read_FASTA(filename::String, n_max=1000)::Dict{Int32, Vector{Int8}}
    """
    Loading sequences in memory as vectors in dictionary
    """
        
    sequences = Dict{Int32, Vector{Int8}}()
    FastaReader(filename) do file                    # Operates both .fasta and .fasta.gz
            for ((_, seq), i) in zip(file, 1:n_max)
                sequences[i] = toint(seq)
            end
        end
    return sequences
end

    
function toint(seq::String, alphabet=
            Dict{Char, Int8}('-'=>0, 'A'=>1, 'C'=>2, 'G'=>3, 'U'=>4))::Vector{Int8}
    """
    in: dryed sequence (alphabet characters only)
    out: vector of numbers
        """
    inted = [alphabet[i] for i in seq]
    return inted
end


function distance(seq1, seq2)::Float32
    common = seq1 .* seq2                  # common positions remain as non-zero
    difference = (seq1 .- seq2) .* common  # same letters turn to zeros
    diff = count(i -> i != 0, difference)  # counting difference
    total = count(i -> i != 0, common)     # counting total
    return total > 0 ? diff/total : 0.0
end


    
function p_matrix(dick)::Matrix{Float32}
    total_comps = (n_max-1) * div(n_max, 2)
    progbar = Progress(total_comps, dt=1) #, showspeed=true)
    
    
    matrix = zeros(Float32, n_max, n_max)
    for i = 1:n_max
        seq1::Vector{Int8} = dick[i]
        for j = i+1:n_max
            seq2::Vector{Int8} = dick[j]
            dist = distance(seq1, seq2)
            matrix[i, j] = dist
            next!(progbar)
        end
    end
    return matrix
end



filename = "/home/uzumymw/data/fasta/silva/archives/4.1silva_onlybacINDEXED_dryest.fasta.gz"
name_out = "/home/uzumymw/data/other/p_matrix20000.csv"
n_max = 20000
dick = read_FASTA(filename, n_max)

function main(dick)
    matrix::Matrix{Float32} = Symmetric(p_matrix(dick))
    println(sum(matrix))
    writedlm( name_out,  matrix, ',')
end



@time begin
main(dick)
end
