# Common functions for simplex computations

module Simplex
"""
    remove(sym_arr::Matrix, n::Int)::Matrix

Removes n-th line and row of square matrix
"""
function remove(sym_arr::Matrix, n::Int)::Matrix
    s = size(sym_arr)[1]
    v = vec(copy(sym_arr))
    return reshape(
        deleteat!(
            deleteat!(v, n:s:s*s),
        (n-1)*(s-1)+1:n*(s-1)),
    (s-1,s-1))
end

function remove(sym_arr::AbstractMatrix, n::Int)::Matrix
    s = size(sym_arr)[1]
    v = vec(Matrix(copy(sym_arr)))
    return reshape(
        deleteat!(
            deleteat!(v, n:s:s*s),
        (n-1)*(s-1)+1:n*(s-1)),
    (s-1,s-1))
end


"""
    getsums(array::Array{Int8})::Vector{UInt16}

Returns sums of columns of sym. matrix
"""
function getsums(array::Array{Int8})::Vector{UInt16}
    s = size(array)[1]
    sums = zeros(UInt16, s)
    for (i, col) in enumerate(eachcol(array))
        sums[i] = sum(col)
    end
    sums
end

end
