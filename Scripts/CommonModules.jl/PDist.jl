# Common functions for p_dist matrix computing

"""
* toint(seq::String, alphabet::Dict)::Vector{Int8}

* read_FASTA(filename::String, n_max=1)::Vector{Vector{Int8}}

* pdist(T::DataType, seq1, seq2)::T

* pdist(T::DataType, dict::Dict, id1, id2)::T

* controlled_twist!(vec::AbstractVector)::Vector{Int64}

* vecsplit_by(vector, n::Int)

"""
module PDist


using FastaIO
using ProgressMeter
using Random
using ThreadsX



"""
    toint(seq::String, alphabet=Dict('-'=>0, 'A'=>1, 'C'=>2, 'G'=>3, 'U'=>4, 'T'=>4))

Converts sequence to vector using given dictionary

# Examples
```jldoctest
julia> toint("AG-CA")
5-element Vector{Int8}:
 1
 3
 0
 2
 1

julia> toint("tree",Dict('t'=>1,'r'=>0,'e'=>-1))
4-element Vector{Int8}:
  1
  0
 -1
 -1
```
"""
@inline function toint(
    seq::String, alphabet=Dict('-'=>0, 'A'=>1, 'C'=>2, 'G'=>3, 'U'=>4, 'T'=>4)
    )
    @inbounds Int8[alphabet[uppercase(i)] for i in seq]
end




"""
    read_FASTA(filename::String, n_max=1)

Reads .fasta (or .fasta.gz) `filename`,
returns `n_max` sequences from top as `Vector`
"""
function read_FASTA(filename::String, n_max=1)
    vec = Vector{Vector{Int8}}(undef, n_max)
    progbar = Progress(
        n_max, desc="Загрузка словаря в память: ", dt=.5, showspeed=true
    )
    FastaReader(filename) do file  # Operates both .fasta and .fasta.gz
        for ((_, seq), i) in zip(file, 1:n_max)
            vec[i] = toint(seq)
            next!(progbar)
        end
    end
    return vec
end



"""
    pdist(T::DataType, seq1, seq2)::T
    pdist(T::DataType, dict::Dict, id1, id2)::T

Calculates p-distance between two vectors of same length
Or takes values from vector via indexes.
"""
function pdist(T::DataType, seq1, seq2)::T
    diff = 0
    total = 0
    @inbounds @simd for i in eachindex(seq1, seq2)
        x = (seq1[i] + seq2[i]) != 0
        diff += x & (seq1[i] != seq2[i])
        total += x
    end
    total > 0 ? diff/total : zero(T)
end

function pdist(T::DataType, vec::Vector, id1::Int, id2::Int)::T
    @inbounds pdist(T, vec[id1], vec[id2])
end



"""
    controlled_twist!(vec::AbstractVector)::Vector{Int64}

Shuffles vector in place and returns vector of indexes

# Example
```jldoctest
julia> a = [10,20,30,40]
4-element Vector{Int64}:
 10
 20
 30
 40

julia> PDist.controlled_twist!(a)
4-element Vector{Int64}:
 1
 4
 3
 2

julia> a
4-element Vector{Int64}:
 10
 40
 30
 20
```
"""
function controlled_twist!(vec::AbstractVector)::Vector{Int64}
    ids = [1:length(vec);]
    rng = MersenneTwister(rand(UInt64))
    Random.shuffle!(copy(rng), vec)
    Random.shuffle!(copy(rng), ids)
    ids
end



"""
    vecsplit_by(vector, n::Int)

Splits vector to n equal parts (+ remainder)

# Example
```jldoctest
julia> PDist.vecsplit_by([1,2,3,4,5], 2)
3-element Vector{Vector{Int64}}:
 [1, 2]
 [3, 4]
 [5]

```
"""
function vecsplit_by(v::Vector, n::Int)
    L = length(v)
    @assert 1 ≤ n ≤ L "n must be between 1 and $L == length(v)"
    splitted = [v[(i-1)*n+1:i*n] for i in 1:(L÷n)]
    (L%n!=0) && push!(splitted, v[length(splitted)*n+1:end])
    return splitted
end



function parceline(T, file_handler, delimeter)::Vector{T}
    ThreadsX.map(x -> parse(T, x), string.(split(readline(file_handler), delimeter)))
end

function parceline!(T, file_handler, destination, delimeter)
    ThreadsX.map!(x -> parse(T, x), destination, string.(split(readline(file_handler), delimeter)))
end






end
